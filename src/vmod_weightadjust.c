/*-
 * Copyright 2017 UPLEX Nils Goroll Systemoptimierung
 *
 * code taken from lib/libvmod_directors/random.c:
 * Portions Copyright (c) 2013-2015 Varnish Software AS
 *
 * All rights reserved.
 *
 * Authors: Nils Goroll <nils.goroll@uplex.de>
 *          Poul-Henning Kamp <phk@FreeBSD.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <limits.h>

#include <pcre.h>

#include "cache/cache.h"

#include "vcl.h"
#include "vsb.h"

#include "cache/cache_backend.h"

#include "vdir.h"
#include "vcc_weightadjust_if.h"
#include "vmod_weightadjust.h"
#include "wadj_thread.h"

#define ERR(ctx, msg)					\
	VRT_fail((ctx), "vmod weightadjust error: " msg)
#define VERR(ctx, fmt, ...)						\
	VRT_fail((ctx), "vmod weightadjust error: " fmt, __VA_ARGS__)

static void
msg(VRT_CTX, const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	if (ctx->method == VCL_MET_INIT) {
		AN(ctx->msg);
		VSB_vprintf(ctx->msg, fmt, args);
	}
	else if (ctx->vsl)
		VSLbv(ctx->vsl, SLT_VCL_Error, fmt, args);
	else
		/* Should this ever happen in vcl_fini() ... */
		VSL(SLT_VCL_Error, NO_VXID, fmt, args);
	va_end(args);
}

static void
priv_vcl_free(VRT_CTX, void *priv)
{
	struct vmod_wadj_vcl	*wa_vcl;

	(void)ctx;
	CAST_OBJ_NOTNULL(wa_vcl, priv, VMOD_WADJ_VCL_MAGIC);

	assert(VTAILQ_EMPTY(&wa_vcl->props));
	AZ(pthread_mutex_destroy(&wa_vcl->mtx));
	FREE_OBJ(wa_vcl);
	AZ(wa_vcl);
}

static const struct vmod_priv_methods priv_vcl_methods[1] = {{
		.magic = VMOD_PRIV_METHODS_MAGIC,
		.type = "vmod_weightadjust_priv_vcl",
		.fini = priv_vcl_free
}};

int
vmod_event_function(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	struct vmod_wadj_vcl	*wa_vcl;

	(void) ctx;

	switch (e) {
	case VCL_EVENT_LOAD:
		ALLOC_OBJ(wa_vcl, VMOD_WADJ_VCL_MAGIC);
		AN(wa_vcl);
		VTAILQ_INIT(&wa_vcl->props);
		AZ(pthread_mutex_init(&wa_vcl->mtx, NULL));
		priv->priv = wa_vcl;
		priv->methods = priv_vcl_methods;
		break;
	case VCL_EVENT_DISCARD:
	case VCL_EVENT_WARM:
	case VCL_EVENT_COLD:
		CAST_OBJ_NOTNULL(wa_vcl, priv->priv, VMOD_WADJ_VCL_MAGIC);
		break;
	default:
		return (0);
	}
	AZ(pthread_mutex_lock(&wa_vcl->mtx));
	wa_vcl->state = e;
	wadj_thr_ctl(wa_vcl, NULL, DEFAULT);
	AZ(pthread_mutex_unlock(&wa_vcl->mtx));
	return (0);
}

/* largely taken from vbp_build_req() */
static char *
wadj_cfg_req(const struct backend *be, VCL_STRING url, VCL_STRING request,
    ssize_t *len)
{
	const size_t reql = request ? strlen(request) + 1 : 0;
	char s[reql];
	char *ret, *tok, *save;
	int c = 0;
	struct vsb *vsb;

	vsb = VSB_new_auto();
	AN(vsb);
	VSB_clear(vsb);
	if (request != NULL && reql > 1) {
		(void) strcpy(s, request);
		assert(strlen(s) < sizeof s);
		tok = strtok_r(s, "\r\n", &save);
		while (tok) {
			if (! strcasecmp(tok, "Connection:"))
				c = 1;
			VSB_cat(vsb, tok);
			VSB_cat(vsb, "\r\n");
			tok = strtok_r(NULL, "\r\n", &save);
		}
		if (!c)
			VSB_cat(vsb, "Connection: close\r\n");
		VSB_cat(vsb, "\r\n");
	} else {
		if (url == NULL || url[0] == '\0')
			url = "/";
		VSB_printf(vsb, "GET %s HTTP/1.1\r\n", url);
		if (be->hosthdr != NULL)
			VSB_printf(vsb, "Host: %s\r\n", be->hosthdr);
		VSB_cat(vsb, "Connection: close\r\n");
		VSB_cat(vsb, "\r\n");
	}
	AZ(VSB_finish(vsb));
	ret = strdup(VSB_data(vsb));
	AN(ret);
	*len = VSB_len(vsb);
	assert(*len >= 0);
	VSB_destroy(&vsb);
	return (ret);
}

//lint -e{818} priv
VCL_VOID
vmod_random__init(VRT_CTX, struct vmod_weightadjust_random **rrp,
    const char *vcl_name, struct vmod_priv *priv)
{
	struct vmod_weightadjust_random *rr;
	struct vmod_wadj_vcl		*wa_vcl;
	AN(priv);
	CAST_OBJ_NOTNULL(wa_vcl, priv->priv, VMOD_WADJ_VCL_MAGIC);

	_vmod_random__init(ctx, rrp, vcl_name);
	rr = *rrp;
	CHECK_OBJ(rr, VMOD_WEIGHTADJUST_RANDOM_MAGIC);

	rr->extra.wa_vcl = wa_vcl;
	VTAILQ_INIT(&rr->extra.props);
}

VCL_VOID
vmod_random__fini(struct vmod_weightadjust_random **rrp)
{
	struct vmod_weightadjust_random *rr;
	struct vmod_wadj_vcl	*wa_vcl;

	rr = *rrp;
	*rrp = NULL;
	CHECK_OBJ_NOTNULL(rr, VMOD_WEIGHTADJUST_RANDOM_MAGIC);
	wa_vcl = rr->extra.wa_vcl;
	CHECK_OBJ_NOTNULL(wa_vcl, VMOD_WADJ_VCL_MAGIC);

	AZ(pthread_mutex_lock(&wa_vcl->mtx));
	wadj_dir_fini(&rr->extra.props, &wa_vcl->props);
	AZ(pthread_mutex_unlock(&wa_vcl->mtx));

	_vmod_random__fini(&rr);
	AZ(rr);
}

//lint -e{818} args
VCL_VOID
vmod_random_add_backend(VRT_CTX,
    struct VPFX(weightadjust_random) *rr,
    struct VARGS(random_add_backend) *args)
{
	struct vmod_wadj_vcl	*wa_vcl;
	struct wadj_prop	*prop;
	unsigned		u, nb;
	struct backend		*be;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(rr, VMOD_WEIGHTADJUST_RANDOM_MAGIC);
	wa_vcl = rr->extra.wa_vcl;
	CHECK_OBJ_NOTNULL(wa_vcl, VMOD_WADJ_VCL_MAGIC);

	if (args->backend->priv == NULL) {
		ERR(ctx, "can only be used with real backends "
		    "(no director layering)");
		return;
	}

	CAST_OBJ_NOTNULL(be, args->backend->priv, BACKEND_MAGIC);

	if (! args->valid_weight_update) {
		msg(ctx, "vmod weightadjust director %s "
		    "add_backend(%s) without weight update falls back "
		    "to standard behaviour",
		    rr->vd->dir->vcl_name, args->backend->vcl_name);
		goto no_weightadjust;
	}

	vdir_rdlock(rr->vd);
	nb = rr->vd->n_backend;
	for (u = 0; u < nb; u++) {
		if (rr->vd->backend[u] == args->backend)
			break;
	}
	vdir_unlock(rr->vd);
	if (u < nb) {
		VERR(ctx, "director %s add_backend(%s): already exists",
		    rr->vd->dir->vcl_name, be->vcl_name);
		return;
	}

	ALLOC_OBJ(prop, WADJ_PROP_MAGIC);
	AN(prop);
	AZ(pthread_mutex_init(&prop->wakeup.mtx, NULL));
	AZ(pthread_cond_init(&prop->wakeup.cv, NULL));
	prop->vd = rr->vd;
	prop->be = be;
	VRT_Assign_Backend(&prop->dir, args->backend);
	prop->vd_updates = &rr->extra.updates;
	AN(args->weight_update);
	prop->vre = args->weight_update;
	prop->req = wadj_cfg_req(be, args->url, args->request, &prop->reqlen);
	if ((prop->timeout = args->timeout) < 0) {
		ERR(ctx, "negative timeout");
		goto err;
	}
	if ((prop->interval = args->interval ) < 0) {
		ERR(ctx, "negative interval");
		goto err;
	}
	if ((prop->bufsz = args->buffer) < 128) {
		ERR(ctx, "buffer too small");
		goto err;
	}

	vdir_wrlock(rr->vd);
	VTAILQ_INSERT_TAIL(&rr->extra.props, prop, list_vd);
	vdir_unlock(rr->vd);

	AZ(pthread_mutex_lock(&wa_vcl->mtx));
	VTAILQ_INSERT_TAIL(&wa_vcl->props, prop, list_vcl);
	wadj_thr_ctl(wa_vcl, prop, START);
	AZ(pthread_mutex_unlock(&wa_vcl->mtx));

  no_weightadjust:
	vdir_add_backend(ctx, rr->vd, args->backend, args->weight);
	return;

  err:
	VRT_Assign_Backend(&prop->dir, NULL);
	FREE_OBJ(prop);
}

VCL_VOID
vmod_random_remove_backend(VRT_CTX,
    struct vmod_weightadjust_random *rr, VCL_BACKEND vcl_be)
{
	struct vmod_wadj_vcl	*wa_vcl;
	struct wadj_prop	*prop;
	struct wadj_prop_head	props;
	struct backend		*be;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(rr, VMOD_WEIGHTADJUST_RANDOM_MAGIC);
	wa_vcl = rr->extra.wa_vcl;
	CHECK_OBJ_NOTNULL(wa_vcl, VMOD_WADJ_VCL_MAGIC);

	if (vcl_be->priv == NULL) {
		ERR(ctx, "can only be used with real backends "
		    "(no director layering)");
		return;
	}

	CAST_OBJ_NOTNULL(be, vcl_be->priv, BACKEND_MAGIC);

	vdir_wrlock(rr->vd);
	VTAILQ_FOREACH(prop, &rr->extra.props, list_vd) {
		if (prop->be == be)
			break;
	}
	if (prop == NULL) {
		vdir_unlock(rr->vd);
		goto no_weightadjust;
	}

	assert(prop->vd == rr->vd);
	VTAILQ_REMOVE(&rr->extra.props, prop, list_vd);
	vdir_unlock(rr->vd);

	VTAILQ_INIT(&props);
	VTAILQ_INSERT_HEAD(&props, prop, list_vd);

	vdir_rdlock(rr->vd);
	AZ(pthread_mutex_lock(&wa_vcl->mtx));
	wadj_dir_fini(&props, &wa_vcl->props);
	AZ(pthread_mutex_unlock(&wa_vcl->mtx));
	vdir_unlock(rr->vd);

  no_weightadjust:
	vdir_remove_backend(ctx, rr->vd, vcl_be, NULL);
}
