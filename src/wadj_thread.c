/*-
 * Copyright 2017 UPLEX Nils Goroll Systemoptimierung
 *
 * code taken from varnish-cache bin/varnishd/cache/cache_backend_probe.c:
 * Portions Copyright (c) 2006 Verdens Gang AS
 * Portions Copyright (c) 2006-2011 Varnish Software AS
 *
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <poll.h>
#include <sys/socket.h>
#include <errno.h>
#include <limits.h>

#include <pcre.h>

#include "cache/cache.h"

#include "vre.h"
#include "vsa.h"
#include "vtim.h"
#include "vtcp.h"
#include "vrnd.h"

#include "cache/cache_backend.h"
#ifdef FIXME
#include "cache/cache_tcp_pool.h"
#else
int VCP_Open(struct conn_pool *, vtim_dur tmo, VCL_IP *, int*);
#endif

#include "vdir.h"

#include "wadj_prop.h"
#include "wadj_thread.h"

#define POKE_ERR(prop, fmt, ...)					  \
	VSL(SLT_VCL_Error, NO_VXID, "vmod weightadjust error: poke %s.%s " fmt, \
	    (prop)->vd->dir->vcl_name, (prop)->be->vcl_name,	  \
	    __VA_ARGS__)

#define VPOKE_TRACE(prop, fmt, ...)					\
	VSL(SLT_Debug, NO_VXID, "vmod weightadjust: poke %s.%s " fmt, 	\
	    (prop)->vd->dir->vcl_name, (prop)->be->vcl_name,	\
	    __VA_ARGS__)

#define POKE_TRACE(prop, str)					\
	VSL(SLT_Debug, NO_VXID, "vmod weightadjust: poke %s.%s " str, \
	    (prop)->vd->dir->vcl_name, (prop)->be->vcl_name)

/* largely copied from vbp_poke */
static void
wadj_poke(const struct wadj_prop *prop, char *buf,
    ssize_t *len)
{
	int s, tmo, err;
	ssize_t i;
	ssize_t rlen;
	double t_start, t_now, t_end;
	struct pollfd pfda[1], *pfd = pfda;
	const struct suckaddr *sa;
	// pcre JIT requires 32K anyway, and we are not running when it is
	char drain[32*1024];

	t_start = t_now = VTIM_real();
	t_end = t_start + prop->timeout;

	*buf = '\0';
	*len = 0;

	s = VCP_Open(prop->be->conn_pool, t_end - t_now, &sa, &err);

	if (s < 0) {
		POKE_ERR(prop, "Open error %d (%s)", err, strerror(err));
		return;
	}

	i = VSA_Get_Proto(sa);
	assert(i == AF_INET || i == AF_INET6);

	t_now = VTIM_real();
	tmo = (int)round((t_end - t_now) * 1e3);
	if (tmo <= 0) {
		POKE_ERR(prop, "connection timeout after %fs",
		    t_now - t_start);
		VTCP_close(&s);
		return;
	}

	assert(prop->reqlen >= 0);
	i = write(s, prop->req, (size_t)prop->reqlen);
	if (i != prop->reqlen) {
		POKE_ERR(prop, "short write (%zd out of %zd bytes)",
		    i, prop->reqlen);
		VTCP_close(&s);
		return;
	}

	pfd->fd = s;
	rlen = 0;
	do {
		pfd->events = POLLIN;
		pfd->revents = 0;
		tmo = (int)round((t_end - t_now) * 1e3);
		if (tmo > 0)
			i = poll(pfd, 1UL, tmo);
		if (i == 0 || tmo <= 0) {
			POKE_ERR(prop, "read timeout after %fs",
			    VTIM_real() - t_start);
			VTCP_close(&s);
			return;
		}
		if (rlen < prop->bufsz)
			i = read(s, buf + rlen, (size_t)(prop->bufsz - rlen));
		else
			i = read(s, drain, sizeof(drain));
		if (i <= 0)
			break;
		if (rlen < prop->bufsz)
			rlen += i;
	} while (1);

	VTCP_close(&s);

	if (rlen < prop->bufsz)
		buf[rlen] = '\0';
	else {
		assert(rlen == prop->bufsz);
		assert(rlen > 0);
		buf[--rlen] = '\0';
	}

	*len = rlen;

	if (i < 0)
		POKE_ERR(prop, "read error: %s", strerror(errno));
}

static void *
wadj_update(void *arg)
{
	struct wadj_prop *pa;
	CAST_OBJ_NOTNULL(pa, arg, WADJ_PROP_MAGIC);
	struct wadj_prop_wakeup *wa = &pa->wakeup;
	const struct wadj_prop *pr = pa;
	struct vdir *vd;
	struct timespec due;
	unsigned u = UINT_MAX;
	txt groups[2];
	int i;
	char *p, *pp;
	AN(pr->bufsz);
	char buf[pr->bufsz];
	ssize_t len;
	double w;

	AN(pr->vd);
	vd = pr->vd;
	AN(pr->be);
	AN(pr->vre);
	AN(pr->req);
	AN(pr->req[0]);

	POKE_TRACE(pr, "starting");

	due.tv_sec = (long)VTIM_real();
	due.tv_nsec = (typeof(due.tv_nsec))(1e9 * VRND_RandomTestableDouble());

	AZ(pthread_mutex_lock(&wa->mtx));
	while (pr->run >= STARTING) {
		i = pthread_cond_timedwait(&wa->cv, &wa->mtx,
		    &due);
		if (i == 0 || i == EINTR)
			continue;
		assert(i == ETIMEDOUT);
		due.tv_sec += (typeof(due.tv_sec))pr->interval;
		wadj_poke(pr, buf, &len);
		if (len == 0)
			continue;
		assert(len > 0);
		assert(len <= pr->bufsz);

		// XXX should use VRE_MATCH_NOTBOL|VRE_MATCH_NOTEOL
		i = VRE_capture(pr->vre, buf, (size_t)len, 0 /*here*/, groups,
		    2UL, NULL);

		if (i < 0) {
			POKE_ERR(pr, "regular expression error %d", i);
			continue;
		}

		if (i != 2) {
			POKE_ERR(pr, "regular expression "
			    "need one caputuring match, got %d", i - 1);
			continue;
		}
		p = (char *)groups[1].b;

		errno = 0;
		w = strtod(p, &pp);
		if (pp == p || w == 0.0 || w == HUGE_VALF || w == HUGE_VALL) {
			POKE_ERR(pr, "conversion error or zero value %f "
			    "(errno %d) for: %s", w, errno, p);
			continue;
		}
		if (w < 0) {
			POKE_ERR(pr, "negative weight %f", w);
			continue;
		}
		vdir_wrlock(vd);
		if (u >= vd->n_backend ||
		    vd->backend[u]->priv != pr->be) {
			/* we got moved around */
			for (u = 0; u < vd->n_backend; u++) {
				if (vd->backend[u]->priv == pr->be)
					break;
			}
			assert(u < vd->n_backend); // assert(found)
		}
		vd->total_weight += w;
		vd->total_weight -= vd->weight[u];
		vd->weight[u] = w;

		/* arbitrary safety measure against precision loss */
		if ((*pr->vd_updates)++ > 128) {
			*pr->vd_updates = 0;
			vd->total_weight = 0;
			for (u = 0; u < vd->n_backend; u++)
				vd->total_weight += vd->weight[u];
			VPOKE_TRACE(pr, "recalc total %f", vd->total_weight);
		}
		VPOKE_TRACE(pr, "set to %f total %f", w, vd->total_weight);
		vdir_unlock(vd);
	}
	AZ(pthread_mutex_unlock(&wa->mtx));
	POKE_TRACE(pr, "stopping");
	return NULL;
}

static void
wadj_thr_start(struct wadj_prop *prop)
{
	pthread_attr_t attr;
	size_t sz;

	if (prop->run >= STARTING)
		return;

	AZ(pthread_mutex_lock(&prop->wakeup.mtx));
	if (prop->run >= STARTING) {
		AZ(pthread_mutex_unlock(&prop->wakeup.mtx));
		return;
	}
	AZ(pthread_attr_init(&attr));
	// 128k jit/general + buf
	assert(prop->bufsz > 0);
	sz = 128 * 1024 + (size_t)prop->bufsz;
	AZ(pthread_attr_setstacksize(&attr, sz));

	prop->run = STARTING;
	AZ(pthread_create(&prop->thread, &attr, wadj_update, prop));
	prop->run = RUNNING;
	AZ(pthread_mutex_unlock(&prop->wakeup.mtx));
	AZ(pthread_attr_destroy(&attr));
}

static void
wadj_thr_stop(struct wadj_prop *prop)
{
	if (prop->run <= STOPPING)
		return;

	AZ(pthread_mutex_lock(&prop->wakeup.mtx));
	if (prop->run <= STOPPING) {
		AZ(pthread_mutex_unlock(&prop->wakeup.mtx));
		return;
	}
	prop->run = STOPPING;
	AZ(pthread_cond_signal(&prop->wakeup.cv));
	AZ(pthread_mutex_unlock(&prop->wakeup.mtx));
	AZ(pthread_join(prop->thread, NULL));
	AZ(pthread_mutex_lock(&prop->wakeup.mtx));
	if (prop->run == STOPPING)
		prop->run = STOPPED;
	AZ(pthread_mutex_unlock(&prop->wakeup.mtx));
}

/* wa_vcl->mtx must be held */
void
wadj_thr_ctl(const struct vmod_wadj_vcl *wa_vcl,
    struct wadj_prop *prop, enum wadj_thr_action_e action)
{
	if (wa_vcl->state == VCL_EVENT_LOAD ||
	    wa_vcl->state == VCL_EVENT_DISCARD)
		return;

	if (prop) {
		if (wa_vcl->state == VCL_EVENT_WARM) {
			if (action == START || action == DEFAULT)
				wadj_thr_start(prop);
			else
				wadj_thr_stop(prop);
			return;
		}

		assert(wa_vcl->state == VCL_EVENT_COLD);
		wadj_thr_stop(prop);
		return;
	}

	assert(prop == NULL);
	assert(action == DEFAULT);

	if (wa_vcl->state == VCL_EVENT_WARM) {
		VTAILQ_FOREACH(prop, &wa_vcl->props, list_vcl) {
			wadj_thr_start(prop);
		}
		return;
	}

	assert(wa_vcl->state == VCL_EVENT_COLD);

	VTAILQ_FOREACH(prop, &wa_vcl->props, list_vcl) {
		wadj_thr_stop(prop);
	}
}

/* detruct prop */
void
wadj_prop_fini(struct wadj_prop **propp)
{
	struct wadj_prop *prop;

	prop = *propp;
	*propp = NULL;

	CHECK_OBJ_NOTNULL(prop, WADJ_PROP_MAGIC);
	assert(prop->run == STOPPED);
	AN(prop->vre);
	AN(prop->req);
	free(prop->req);
	AZ(pthread_cond_destroy(&prop->wakeup.cv));
	AZ(pthread_mutex_destroy(&prop->wakeup.mtx));
	FREE_OBJ(prop);
}

/*
 * stop all threads and fini all props of a director
 *
 * removes only from vcl_prop, not from vd_props
 *
 * wa_vcl->mtx must be held
 */
void
wadj_dir_fini(const struct wadj_prop_head *vd_props,
	      struct wadj_prop_head *vcl_props)
{
	struct wadj_prop	*prop, *save;

	//lint -e{850} loop variable modified
	VTAILQ_FOREACH_SAFE(prop, vd_props, list_vd, save) {
		VTAILQ_REMOVE(vcl_props, prop, list_vcl);
		VRT_Assign_Backend(&prop->dir, NULL);
		wadj_thr_stop(prop);
		wadj_prop_fini(&prop);
		AZ(prop);
	}
}
