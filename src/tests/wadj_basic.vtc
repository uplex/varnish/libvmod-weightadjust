varnishtest "weightadjust basic"

server s1 {
	rxreq
	expect req.url == "/"
	expect req.http.Connection == "close"
	txresp -hdr "X-Weight: 11.345whatever"
	accept
	rxreq
	expect req.url == "/"
	expect req.http.Host == "Host"
	expect req.http.Connection == "close"
	txresp -hdr "X-Weight: 12.345whatever"
} -start

server s2 {
	rxreq
	expect req.url == "/otherurl"
	expect req.http.Connection == "close"
	txresp -body "127ewsdfinthebodyljgof5te3.789sdh723"
} -start

server s3 {
	rxreq
	expect req.url == "/weight"
	expect req.http.Host == "weighthost"
	expect req.http.Connection == "close"
	txresp -hdr "X-Weight: 42something"
} -start

varnish v1 -vcl+backend {
	import ${vmod_weightadjust};
	import vtc;

	backend s1_hosthdr {
	    .host = "${s1_addr}";
	    .port = "${s1_port}";
	    .host_header = "Host";
	}

	sub vcl_init {
	    new foo = weightadjust.random();
	    foo.add_backend(backend=s1, weight=1,
			    # XXX add back ^X-Weight
			    weight_update = "X-Weight: ([\d.]+)",
			    interval = 1h);
	}

	sub vcl_recv {
	    if (req.url == "/1") {
		vtc.sleep(1s);
		foo.add_backend(backend=s1_hosthdr, weight=1,
				# XXX add back ^X-Weight
				weight_update = "X-Weight: (\d+\.\d+)",
				interval = 1h);
		vtc.sleep(1s);
		foo.remove_backend(backend=s1);
		return (synth(200));
	    }
	    if (req.url == "/2") {
		foo.add_backend(backend=s2, weight=1,
				url = "/otherurl",
				weight_update = "inthebody.*(\d+\.\d+)",
				interval = 1h);
		vtc.sleep(1s);
		return (synth(200));
	    }
	    if (req.url == "/3") {
		foo.add_backend(backend=s3, weight=1,
				request={"
GET /weight HTTP/1.1
Host: weighthost
"},
				# XXX add back ^X-Weight
				weight_update = "X-Weight: ([\d.]+)",
				interval = 1h);
		vtc.sleep(1s);
		return (synth(200));
	    }
	    return (synth(400));
	}
} -start

logexpect l1 -v v1 -g raw -d 1 {
    expect * *    Debug	{^vmod weightadjust: poke foo.s1 starting}
    expect * *    Debug	{^vmod weightadjust: poke foo.s1 set to 11.345000 total 11.345000}
    expect * *    Debug	{^vmod weightadjust: poke foo.s1_hosthdr starting}
    expect * *    Debug	{^vmod weightadjust: poke foo.s1_hosthdr set to 12.345000 total 23.690000}
    expect * *    Debug	{^vmod weightadjust: poke foo.s1 stopping}
    expect * *    Debug	{^vmod weightadjust: poke foo.s2 starting}
    expect * *    Debug	{^vmod weightadjust: poke foo.s2 set to 3.789000 total 16.134000}
    expect * *    Debug	{^vmod weightadjust: poke foo.s3 starting}
    expect * *    Debug	{^vmod weightadjust: poke foo.s3 set to 42.000000 total 58.134000}
} -start

client c1 {
	txreq -url "/1"
	rxresp
	txreq -url "/2"
	rxresp
	txreq -url "/3"
	rxresp
} -run

logexpect l1 -wait
