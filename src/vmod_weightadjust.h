/*-
 * Copyright 2017 UPLEX Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "wadj_prop.h"

struct vmod_wadj_vcl;

struct vmod_wadj_extra {
	struct vmod_wadj_vcl		*wa_vcl;
	struct wadj_prop_head		props;
	int				updates;
};

struct vmod_weightadjust_random {
	unsigned			magic;
#define VMOD_WEIGHTADJUST_RANDOM_MAGIC	0xea90ee21
	struct vdir			*vd;
	struct vmod_wadj_extra		extra;
};

/*
 * code from varnish-cache vmod_directors random.c
 *
 * original symbol-names #define replaced to those used here
 */
VCL_VOID _vmod_random__init(VRT_CTX, struct vmod_weightadjust_random **rrp,
    const char *vcl_name);
VCL_VOID _vmod_random__fini(struct vmod_weightadjust_random **rrp);
