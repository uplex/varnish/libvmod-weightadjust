/*-
 * Copyright 2017 UPLEX Nils Goroll Systemoptimierung
 *
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef NO_VXID
#define NO_VXID (0U)
#endif

enum run_state_e {
	STOPPED	= 0,
	STOPPING,
	STARTING,
	RUNNING
};

struct wadj_prop_wakeup {
	pthread_mutex_t	mtx;
	pthread_cond_t		cv;
};

struct wadj_prop {
	unsigned		magic;
#define WADJ_PROP_MAGIC		0xa05991ff

	VTAILQ_ENTRY(wadj_prop)	list_vd;
	VTAILQ_ENTRY(wadj_prop)	list_vcl;

	struct vdir		*vd;
	struct backend		*be;
	VCL_BACKEND		dir;
	int			*vd_updates;

	pthread_t		thread;
	struct wadj_prop_wakeup wakeup;
	enum run_state_e	run;
	VCL_REGEX		vre;
	char			*req;
	ssize_t			reqlen;
	VCL_DURATION		timeout;
	VCL_DURATION		interval;
	VCL_INT		bufsz;
};

struct vmod_wadj_vcl {
	unsigned		magic;
#define VMOD_WADJ_VCL_MAGIC	0xc125c9c3

	struct wadj_prop_head	props;
	pthread_mutex_t	mtx;
	enum vcl_event_e	state;
};

enum wadj_thr_action_e {
	STOP,
	START,
	DEFAULT
};

void wadj_thr_ctl(const struct vmod_wadj_vcl *wa_vcl,
    struct wadj_prop *prop, enum wadj_thr_action_e action);
void wadj_dir_fini(const struct wadj_prop_head *vd_props,
    struct wadj_prop_head *vcl_props);
