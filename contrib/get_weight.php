<?php

exec('uptime', $output);

// get result
$load = [0 => 0];
if (preg_match('/average: ([0-9]+\.[0-9]{2}), ([0-9]+\.[0-9]{2}), ([0-9]+\.[0-9]{2})$/', $output[0], $result)) {
    $load[0] = (float)$result[1];
    $load[1] = (float)$result[2];
    $load[2] = (float)$result[3];
}

                if ($load[0] < 0.5) {
                        $currentLoad = 100;
                }
                elseif ($load[0] < 0.7) {
                        $currentLoad = 95;
                }
                elseif ($load[0] < 1.0) {
                        $currentLoad = 90;
                }
                elseif ($load[0] < 1.5) {
                        $currentLoad = 80;
                }
                elseif ($load[0] < 2.0) {
                        $currentLoad = 70;
                }
                elseif ($load[0] < 2.5) {
                        $currentLoad = 60;
                }
                elseif ($load[0] < 3.5) {
                        $currentLoad = 50;
                }
                elseif ($load[0] < 4.5) {
                        $currentLoad = 50;
                }
                elseif ($load[0] < 5.0) {
                        $currentLoad = 40;
                }
                elseif ($load[0] < 6.0) {
                        $currentLoad = 30;
                }
                elseif ($load[0] < 7.0) {
                        $currentLoad = 20;
                }
                elseif ($load[0] < 8.0) {
                        $currentLoad = 10;
                }
                else {
                        $currentLoad = 1;
                }
header('X-Weight: '.number_format($currentLoad, 2));
echo @php_uname('n') . "\r\n" . 'Weight: '.$currentLoad;
